import 'phaser';
import GamePlay from "./scenes/GamePlay";
import GameMenu from "./scenes/GameMenu";
import GameLevelUp from "./scenes/GameLevelUp";
import GameOver from "./scenes/GameOver";
import CONFIG from "../config";

let game : Phaser.Game;
let gameOptions = {

    // world gravity
    gravity: 4,
}

window.onload = function() {

    let gameConfig = {
        // render type: let the game decide if CANVAS of WEBGL
        type: Phaser.AUTO,

        // width of the game, in pixels
        width: 800,

        // height of the game, in pixels
        height: 800*16/9,

        // background color (black)
        backgroundColor: 0xB6D3FF,

        // physics settings
        physics: {

            // we are using Matter JS
            default: "matter",
            matter: {

                // gravity settings
                gravity: {
                    x: 0,
                    y: gameOptions.gravity
                }
            }
        },
        scene: [GamePlay, GameLevelUp, GameOver],

    }
    game = new Phaser.Game(gameConfig);
    window.focus();
    resize();
    window.addEventListener("resize", resize, false);

}

function resize(){
    let canvas = document.querySelector("canvas");
    let windowWidth = window.innerWidth;
    let windowHeight = window.innerHeight;
    let windowRatio = windowWidth / windowHeight;
    let gameRatio = Number(game.config.width) / Number(game.config.height);
    if(windowRatio < gameRatio){
        canvas.style.width = windowWidth + "px";
        canvas.style.height = (windowWidth / gameRatio) + "px";
    }
    else{
        canvas.style.width = (windowHeight * gameRatio) + "px";
        canvas.style.height = windowHeight + "px";
    }
}