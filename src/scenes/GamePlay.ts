import 'phaser';
import CONFIG from "../../config";

// global game options
let gameOptions = {

    // world gravity
    gravity: 4,

    // ball horizontal speed
    ballSpeed: 4,

    // jump force
    jumpForce: 30,

    // amount of bars each wall is divided in
    bars: 4,

    // array with the colors to pick from
    barColors: [0x1abc9c, 0x2980b9, 0x9b59b6, 0xf1c40f, 0xc0392b, 0xecf0f1],

    localStorageName: "ColorJumpScore"
}

// constants used to pass "LEFT" and "RIGHT" as arguments rather than "0" and "1"
const LEFT = 0;
const RIGHT = 1;
const WATING = 0;
const PLAY = 1;

export default class GamePlay extends Phaser.Scene{
    leftWalls : any;
    rightWalls : any;
    ball : any;
    coin : any;
    scoreText: Phaser.GameObjects.BitmapText;
    bestScoreText: Phaser.GameObjects.BitmapText;
    bestScore: any;
    logo: Phaser.GameObjects.Sprite;
    score: number;
    gameState: number;
    playButton: Phaser.GameObjects.Sprite;

    constructor(){
        super("GamePlay");
        this.gameState = WATING;
    }

    // preloading assets
    preload(){
        this.load.image("wall", "assets/sprites/wall.png");
        this.load.image("ball", "assets/sprites/ball.png");
        this.load.image("coin", "assets/sprites/coin.png");

        this.load.image("logo", "assets/sprites/Banner_hifpt.png");
        this.load.image("gameover", "assets/sprites/GameOver.png");
        this.load.image("play", "assets/sprites/play.png");
        this.load.image("scorepanel", "assets/sprites/scorepanel.png");
        this.load.image("scorelabels", "assets/sprites/scorelabels.png");
        this.load.bitmapFont("font", "assets/fonts/font.png", "assets/fonts/font.fnt");
    }

    // method to be executed once, when the scene has been created
    create(){

        this.score = 0;
        this.bestScore = localStorage.getItem(gameOptions.localStorageName) == null ? 0 : Number(localStorage.getItem(gameOptions.localStorageName));

        let x = 190
        let y = 10
        var scorelabels = this.add.sprite(x, y, "scorelabels");
        scorelabels.setOrigin(0,0);
        scorelabels.setScale(0.7);
        var scorepanel = this.add.sprite(x, y+30, "scorepanel");
        scorepanel.setOrigin(0,0);
        scorepanel.setScale(0.7);
        this.scoreText = this.add.bitmapText( x+15, y+45, "font", "0", 40);
        this.bestScoreText = this.add.bitmapText( x+250, y+45, "font", this.bestScore.toString(), 40);
        this.scoreText.setOrigin(0, 0);

        this.logo = this.add.sprite(this.scale.width / 2, this.scale.height -10, "logo");
        this.logo.setOrigin(0.5, 1);
        this.logo.setScale(0.9);

        if(this.gameState == PLAY){
             // arrays where to store left and right walls
            this.leftWalls = [];
            this.rightWalls = [];

            // each wall is made by "gameOptions.bars" pieces, so we actually have "gameOptions.bars" walls each side
            for(let i = 0; i < gameOptions.bars; i++){

                // adding left and right walls
                this.leftWalls[i] = this.addWall(i, LEFT);
                this.rightWalls[i] = this.addWall(i, RIGHT);
            }
            // adding the ball
            this.ball = this.matter.add.image(this.scale.width / 8, this.scale.height / 8, "ball");

            // setting ball body as circular
            this.ball.setCircle()

            // adding the coin, no matter where, we'll set its position later
            this.coin = this.matter.add.image(0, 0, "coin");

            // setting coin body as circular
            this.coin.setCircle();

            // setting coin body as static (not affected by gravity or collisions)
            this.coin.setStatic(true);

            // setting coin body as sensor. Will fire collision events without actually collide
            this.coin.body.isSensor = true;

            // giving the coin a label called "coin"
            this.coin.body.label = "coin"

            // this method will randomly place the coin
            this.placeCoin();

            // setting ball velocity (horizontal, vertical)
            this.ball.setVelocity(gameOptions.ballSpeed, 0);
        
            // waiting for pointer down input to call "jump" method
            this.input.on("pointerdown", this.jump, this);

            // waiting for a "collisionstart" event. "e" is the event, "b1" and "b2" the bodies
            this.matter.world.on("collisionstart", function (e, b1, b2) {

                // checking b1 and b2 labels to be "leftwall"
                if(b1.label == "leftwall" || b2.label == "leftwall"){

                    // handling collisions on the LEFT side
                    this.handleWallCollision(LEFT, b1, b2);
                }

                // checking b1 and b2 labels to be "rightwall"
                if(b1.label == "rightwall" || b2.label == "rightwall"){

                    // handling collisions on the RIGHT side
                    this.handleWallCollision(RIGHT, b1, b2);
                }

                // checking b1 and b2 labels to be "coin"
                if(b1.label == "coin" || b2.label == "coin"){

                    this.score += 30;
                    if(this.bestScore < this.score)
                    {
                        this.bestScore = this.score;
                        localStorage.setItem(gameOptions.localStorageName, this.bestScore.toString());
                    }
                    this.scoreText.text = this.score.toString();
                    this.bestScoreText.text = this.bestScore.toString();

                    // calling the method to move the coin elsewhere
                    this.placeCoin();
                }
            }, this);
            this.playButton.setVisible(false);
        }
        else{
            var text = this.add.text(this.scale.width/2 , this.scale.height/4, "Welcome to", {
				font: "bold 70px Arial",
                color: "#6e43d9"
			});
			text.setOrigin(0.5, 1);
            var text = this.add.text(this.scale.width/2 , this.scale.height/4+100, "Color Jump Game", {
				font: "bold 70px Arial",
                color: "#6e43d9"
			});
			text.setOrigin(0.5, 1);
            this.playButton = this.add.sprite(this.scale.width/2, this.scale.height/2, "play");
            this.playButton.setScale(0.7)
            this.playButton.setOrigin(0.5,0.5);
            this.playButton.setInteractive();
            this.playButton.on("pointerdown", function(){
                this.scene.start("GamePlay");
                this.gameState = PLAY;
            }, this)
        }

    }
    displayGameOver(){
        this.cameras.main.shake(800, 0.01);
        let gameover = this.add.sprite(this.scale.width / 2, this.scale.height/2-200, "gameover");
        gameover.setOrigin(0.5, 0.5);
        this.gameState = WATING;
        this.playButton.destroy();
        this.playButton = this.add.sprite(this.scale.width/2, this.scale.height/2, "play");
            this.playButton.setScale(0.7)
            this.playButton.setOrigin(0.5,0.5);
            this.playButton.setInteractive();
            this.playButton.on("pointerdown", function(){
                this.scene.start("GamePlay");
                this.gameState = PLAY;
            }, this)
    }
    
    // method to add a wall, given its number (0 = top) and it side
    addWall(wallNumber, side){

        // getting "wall" preloaded image
        let wallTexture = this.textures.get("wall");

        // determining wall height according to game height and the number of bars
        let wallHeight = (this.scale.height-250) / gameOptions.bars;

        // determining wall x position
        let wallX = side * this.scale.width + wallTexture.source[0].width / 2 - wallTexture.source[0].width * side;

        // determining wall y position
        let wallY = 100+ wallHeight * wallNumber + wallHeight / 2;

        // adding the wall
        let wall = this.matter.add.image(wallX, wallY, "wall") as any;

        // the wall is static
        wall.setStatic(true);

        // giving the wall the proper label
        wall.body.label = (side == RIGHT) ? "rightwall" : "leftwall"

        // setting wall height
        wall.displayHeight = wallHeight;

        // returning the wall object
        return wall
    }

    // method to place the coin
    placeCoin(){

        // just placing the coin in a random position between 20% and 80% of the game size
        this.coin.x = Phaser.Math.Between(this.scale.width * 0.2, this.scale.width * 0.8);
        this.coin.y = Phaser.Math.Between(this.scale.height * 0.2, this.scale.height * 0.8);
    }

    // method to handle ball Vs wall collision
    handleWallCollision(side, bodyA, bodyB){
        if(this.gameState == PLAY){
            // if the ball and the wall have different colors...
            if(bodyA.color != bodyB.color){

                // restart the game

                this.displayGameOver();
                // this.scene.start("GamePlay");
            }
            else{
                this.score += 10;
                if(this.bestScore < this.score)
                {
                    this.bestScore = this.score;
                    localStorage.setItem(gameOptions.localStorageName, this.bestScore.toString());
                }
                this.scoreText.text = this.score.toString();
                this.bestScoreText.text = this.bestScore.toString();
            }

            // calling a method to paint the walls
            this.paintWalls((side == LEFT) ? this.rightWalls : this.leftWalls);

            // updating ball velocity
            this.ball.setVelocity(gameOptions.ballSpeed, this.ball.body.velocity.y);
        }
    }

    // method to paint the walls, in the argument the array of walls
    paintWalls(walls){

        // looping through all walls
        walls.forEach(function(wall){

            // picking a random color
            let color = Phaser.Math.RND.pick(gameOptions.barColors);

            // tinting the wall
            wall.setTint(color);

            // also assigning the wall body a custom "color" property
            wall.body.color = color;
        });

        // picking a random wall
        let randomWall = Phaser.Math.RND.pick(walls) as any;

        // painting the ball with the same color used by the random wall
        this.ball.setTint(randomWall.body.color);

        // also assigning the ball body a custom "color" property
        this.ball.body.color = randomWall.body.color;
    }

    // method to jump
    jump(){
        if(this.gameState == PLAY){
            // setting new ball velocity
            this.ball.setVelocity((this.ball.body.velocity.x > 0) ? gameOptions.ballSpeed : -gameOptions.ballSpeed, -gameOptions.jumpForce);
        }
    }

    // method to be called at each frame
    update(){
        if(this.gameState == PLAY){
            // updating ball velocity
            this.ball.setVelocity((this.ball.body.velocity.x > 0) ? gameOptions.ballSpeed : -gameOptions.ballSpeed, this.ball.body.velocity.y);

            // if the ball flies off the screen...
            if(this.ball.y > this.scale.height-150){

                // restart the game
                this.displayGameOver();
                // this.scene.start("GamePlay");
            }
        }
        
    }
};


